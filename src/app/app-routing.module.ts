import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'weather-board',
    pathMatch: 'full',
  },
  { path: 'weather-board',
    loadChildren: () => import('./weather-board/weather-board.module').then(m => m.WeatherBoardModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    preloadingStrategy: PreloadAllModules,
  })],
  exports: [RouterModule],
})
export class AppRoutingModule { }
