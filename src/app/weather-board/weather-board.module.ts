import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WeatherBoardRoutingModule } from './weather-board-routing.module';
import { WeatherHostComponent } from './weather-host/weather-host.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';

import {
  MatButtonModule, MatProgressSpinnerModule,
} from '@angular/material';
import { WeatherChartComponent } from './weather-chart/weather-chart.component';
import { CitySearchComponent } from './city-search/city-search.component';
import { StuctureDirectiveSliderTestComponent } from './stucture-directive-slider-test/stucture-directive-slider-test.component';
import { CustomSliderDirective } from './stucture-directive-slider-test/custom-slider.directive';
import { CdkTestComponent } from './cdk-test/cdk-test.component';
import { CdkTableModule } from '@angular/cdk/table';

@NgModule({
  declarations: [
    WeatherHostComponent, WeatherChartComponent, CitySearchComponent,
    StuctureDirectiveSliderTestComponent, CustomSliderDirective, CdkTestComponent],
  imports: [
    CommonModule,
    WeatherBoardRoutingModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    NgxChartsModule,
    CdkTableModule,
  ],
})
export class WeatherBoardModule { }
