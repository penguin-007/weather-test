import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { fromEvent } from 'rxjs';
import { map, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { TestDataTransportService } from 'src/app/shared/services/test-data-transport.service';

@Component({
  selector: 'app-city-search',
  templateUrl: './city-search.component.html',
  styleUrls: ['./city-search.component.scss']
})
export class CitySearchComponent implements OnInit {

  @ViewChild('search', { static: false })
  private search: ElementRef;

  constructor(
    private _TestDataTransportService: TestDataTransportService,
  ) {
  }

  ngOnInit() {

    this._TestDataTransportService.getData()
    .subscribe((r) => {
      console.log('CitySearchComponent', r);
    }, (error) => {
      console.log(error);
    }, () => console.log('CitySearchComponent completed.'));
  }

  ngAfterViewInit() {
    // this.startSearchListening();
  }

  startSearchListening(input) {
    // const test = fromEvent(this.search.nativeElement, 'keyup');
    console.log(input.target.value);

    // .pipe(
    //   debounceTime(500), // в самом начале
    //   map((event: any) => event.target.value),
    //   distinctUntilChanged(), // после получения значения от map
    // )
    // .subscribe((result) => {
    //   console.log('result', result);
    // });
  }

}
