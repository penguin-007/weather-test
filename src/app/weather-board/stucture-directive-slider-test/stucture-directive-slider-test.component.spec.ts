import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StuctureDirectiveSliderTestComponent } from './stucture-directive-slider-test.component';

describe('StuctureDirectiveSliderTestComponent', () => {
  let component: StuctureDirectiveSliderTestComponent;
  let fixture: ComponentFixture<StuctureDirectiveSliderTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StuctureDirectiveSliderTestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StuctureDirectiveSliderTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
