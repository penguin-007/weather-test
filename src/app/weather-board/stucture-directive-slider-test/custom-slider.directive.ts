import { Directive, Input, AfterViewInit, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appCustomSlider]',
})
export class CustomSliderDirective implements AfterViewInit, OnInit {

  // by guide https://netbasal.com/understanding-angular-structural-directives-659acd0f67e

  context = null;
  index = 0;

  // tslint:disable-next-line: no-input-rename
  @Input('appCustomSliderFrom')
  images: string[];

  @Input()
  appCustomSliderAutoplay: boolean;

  constructor(
    private tpl: TemplateRef<void>,
    private vcr: ViewContainerRef) {
  }

  ngOnInit(): void {
    this.context = {
      $implicit: this.images[0],
      controller: {
        next: () => this.next(),
        prev: () => this.prev(),
      },
    };

    this.vcr.createEmbeddedView(this.tpl, this.context);
  }

  ngAfterViewInit(): void {
    console.log('images', this.images);
    console.log('appCustomSliderAutoplay', this.appCustomSliderAutoplay);
  }

  next() {
    // tslint:disable-next-line: no-increment-decrement
    this.index++;
    if (this.index >= this.images.length) {
      this.index = 0;
    }
    this.context.$implicit = this.images[this.index];
  }

  prev() {
    // tslint:disable-next-line: no-increment-decrement
    this.index--;
    if (this.index < 0) {
      this.index = this.images.length - 1;
    }
    this.context.$implicit = this.images[this.index];
  }

}
