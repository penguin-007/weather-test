import { WeatherForecastService } from './../../shared/services/weather-forecast/weather-forecast.service';
import { Component, OnInit, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { tap, map, takeUntil, catchError, finalize, retry } from 'rxjs/operators';
import { Subject, of, throwError } from 'rxjs';
import { TestDataTransportService } from 'src/app/shared/services/test-data-transport.service';

@Component({
  selector: 'app-weather-chart',
  templateUrl: './weather-chart.component.html',
  styleUrls: ['./weather-chart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WeatherChartComponent implements OnInit, OnDestroy {

  cityId: number;
  lang: string;
  units: string;

  loading = false;

  componentDestroyed$ = new Subject();

  cityName: string;

  // chart config
  yAxisLabel = 'Temperature';
  xAxisLabel = 'Days';

  weatherData = [];

  constructor(
    private _ChangeDetectorRef: ChangeDetectorRef,
    private _WeatherForecastService: WeatherForecastService,
    private _TestDataTransportService: TestDataTransportService,
  ) {
    // config
    this.cityId = 709930;
    this.lang = 'ru';
    this.units = 'metric';
  }

  ngOnInit() {
    this.getWeatherForecast();
  }

  ngOnDestroy() {
    this.componentDestroyed$.next();
    this.componentDestroyed$.complete();
  }

  getWeatherForecast() {
    this.loading = true;

    this._WeatherForecastService.getWeatherForecast(this.cityId, this.lang, this.units)
    .pipe(
      catchError(err => {
        // отлавливает ошибку и пробрасывает дальше
        return throwError(err);
      }),
      finalize(() => {
        // сработает в любом случае в самом конце
        console.log('finalize222');
      }),
      // выполение повторной попытки 3 раза
      // в случае возникновения ошибки
      retry(3),
      tap((result: any) => {
        this.loading = false;
        this.cityName = result.city.name;
      }),
      map((result: any) => {
        return result.list;
      }),
      map((list: any) => {
        return list.map((item) => {
          const date = new Date(item.dt_txt);
          return {
            date: new Date(`${date.getUTCFullYear()}/${date.getUTCMonth() + 1}/${date.getDate()}`),
            value: item.main.temp,
          };
        });
      }),
      takeUntil(this.componentDestroyed$),
    )
    .subscribe((result: any) => {
      console.log('HTTP response', result);
      this.prepareData(result);
      this._ChangeDetectorRef.markForCheck();
    }, (error) => {
      this.handleError(error);
    }, () => console.log('HTTP request completed.'));
  }

  clickbtn() {
    this._TestDataTransportService.setData(Math.round(Math.random() * 10));
  }

  /**
   * Преобразование данных для отображения на графике
   * @param result -
   */
  prepareData(result: any) {
    if (!result.length) {
      this.weatherData = [];
      return;
    }

    const series = [];
    const groupByDate = {};

    result.forEach((item) => {
      if (groupByDate.hasOwnProperty(item.date)) {
        groupByDate[item.date].push(item.value);
      } else {
        groupByDate[item.date] = [item.value];
      }
    });

    for (const item in groupByDate) {
      if (groupByDate.hasOwnProperty(item)) {
        const values = groupByDate[item];
        series.push({
          name: item,
          value: this.getAverage(values),
          min: +Math.min(...values).toFixed(1),
          max: +Math.max(...values).toFixed(1),
        });
      }
    }

    this.weatherData = [
      {
        series,
        name: 'Temperature',
      },
    ];
  }

  /**
   * Вернуть среднее значение массива
   * @param array -
   */
  getAverage(array: number[]) {
    const sum: any = array.reduce((a: number, b: number) => a + b, 0);
    return (sum / array.length).toFixed(1);
  }

  /**
   * Отлов ошибок
   * @param error -
   */
  handleError(error: Error) {
    console.error(error);
  }

}
