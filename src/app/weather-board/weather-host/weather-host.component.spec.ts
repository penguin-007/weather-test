import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeatherHostComponent } from './weather-host.component';

describe('WeatherHostComponent', () => {
  let component: WeatherHostComponent;
  let fixture: ComponentFixture<WeatherHostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeatherHostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherHostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
