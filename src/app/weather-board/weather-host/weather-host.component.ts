import { WeatherForecastService } from './../../shared/services/weather-forecast/weather-forecast.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-weather-host',
  templateUrl: './weather-host.component.html',
  styleUrls: ['./weather-host.component.scss'],
})
export class WeatherHostComponent implements OnInit {

  constructor(
  ) {
  }

  ngOnInit() {
  }

}
