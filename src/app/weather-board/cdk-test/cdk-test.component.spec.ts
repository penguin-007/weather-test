import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CdkTestComponent } from './cdk-test.component';

describe('CdkTestComponent', () => {
  let component: CdkTestComponent;
  let fixture: ComponentFixture<CdkTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CdkTestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CdkTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
