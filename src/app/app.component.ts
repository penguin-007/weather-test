import { TestDataTransportService } from './shared/services/test-data-transport.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'weather-forecast';

  constructor(
    private _TestDataTransportService: TestDataTransportService,
  ) { }

  ngOnInit() {
    setTimeout(() => {
      console.log('add subscribe');
      this._TestDataTransportService.getData().subscribe((r) => {
        console.log('AppComponent', r);
      });
    }, 11000);
  }

}
