import { TestBed } from '@angular/core/testing';

import { TestDataTransportService } from './test-data-transport.service';

describe('TestDataTransportService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TestDataTransportService = TestBed.get(TestDataTransportService);
    expect(service).toBeTruthy();
  });
});
