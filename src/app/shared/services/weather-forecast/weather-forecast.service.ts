import { settings } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class WeatherForecastService {

  constructor(
    private http: HttpClient,
  ) { }

  /**
   * Получить прогноз на 5 дней
   */
  getWeatherForecast(cityID: number, lang: string, units: string) {
    let params = new HttpParams();
    params = params.set('id', cityID.toString());
    params = params.set('lang', lang);
    params = params.set('APPID', settings.WEATHER_API.key);
    params = params.set('units', units);

    return this.http.get(
      `${settings.WEATHER_API.url}/data/2.5/forecast`,
      {
        params,
      },
    );
  }

}
