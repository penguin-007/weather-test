import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject, ReplaySubject, AsyncSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class TestDataTransportService {

  private dataTransfer$ = new Subject<any>();

  constructor(
  ) {
    setTimeout(() => {
      this.dataTransfer$.complete();
      console.log('connection closed', this.dataTransfer$);
    }, 20000);
  }

  getData() {
    return this.dataTransfer$.asObservable();
  }

  setData(data) {
    this.dataTransfer$.next(data);
  }
}
